resource "kubernetes_namespace_v1" "ingress_nginx" {
  metadata {
    name = "ingress-nginx"
  }
}
resource "kubectl_manifest" "wildcard-tls" {
  wait            = true
  validate_schema = false

  yaml_body = yamlencode({
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "Certificate"
    "metadata" = {
      "name"      = "wildcard-tls"
      "namespace" = kubernetes_namespace_v1.ingress_nginx.metadata[0].name
    }
    "spec" = {
      "secretName" = "wildcard-tls"
      "issuerRef" = {
        "name" = yamldecode(kubectl_manifest.cert-manager-cluster-issuer.yaml_body_parsed).metadata.name
        "kind" = "ClusterIssuer"
      }
      "dnsNames" = [
        "*.cloudinterplay.com"
      ]
    }
  })
}
resource "helm_release" "ingress_nginx" {
  name       = "ingress-nginx"

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.10.1"
  namespace  = kubernetes_namespace_v1.ingress_nginx.metadata[0].name
  timeout    = 600

  values = [
    yamlencode({
      controller = {
        tcp = {
          22 = "gitlab/gitlab-gitlab-shell:22"
        }
        extraArgs = {
          default-ssl-certificate: join("/",[
            yamldecode(kubectl_manifest.wildcard-tls.yaml_body_parsed).metadata.namespace,
            yamldecode(kubectl_manifest.wildcard-tls.yaml_body_parsed).spec.secretName])
        }
        ingressClassResource = {
          name = "ingress-nginx"
          controllerValue = "k8s.io/ingress-nginx"
        }
        ingressClassByName = true
        ingressClass = "ingress-nginx"
        replicaCount = "1"
        service = {
          externalTrafficPolicy = "Local"
        }
        admissionWebhooks = {
          enabled = false
        }
      }
    })
  ]
}
