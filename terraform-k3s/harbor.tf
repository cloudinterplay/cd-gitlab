resource "kubernetes_namespace_v1" "harbor" {
  metadata {
    name = "harbor"
  }
}
resource "helm_release" "harbor" {
  chart      = "harbor"
  name       = "harbor"
  namespace  = kubernetes_namespace_v1.harbor.metadata[0].name
  repository = "https://helm.goharbor.io"
  version    = "1.14.2"
  timeout    = 600
  values = [
    yamlencode({
      externalURL = "https://harbor.cloudinterplay.com"
      expose = {
        tls = {
          enabled = false
        }
        ingress = {
          className = "ingress-nginx"
          hosts = {
            core = "harbor.cloudinterplay.com"
            notary: "notary.cloudinterplay.com"
          }  
        }
      }
    })
  ]
}
