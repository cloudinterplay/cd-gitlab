resource "kubernetes_namespace_v1" "metallb" {
  metadata {
    name = "metallb-system"
  }
}
resource "helm_release" "metallb" {
  name       = "metallb"
  namespace  = kubernetes_namespace_v1.metallb.id
  repository = "https://metallb.github.io/metallb"
  chart      = "metallb"
  version    = "0.14.5"
}
resource "kubectl_manifest" "metallb-ipaddress" {
  depends_on = [
    helm_release.metallb
  ]
  wait            = true
  validate_schema = false

  yaml_body = yamlencode({
    apiVersion = "metallb.io/v1beta1"
    kind       = "IPAddressPool"
    metadata = {
      name      = "homelab-ip"
      namespace = kubernetes_namespace_v1.metallb.id
    }
    spec = {
      addresses = [
        "192.168.5.122 - 192.168.5.123"
      ]
    }
  })
}

resource "kubectl_manifest" "metallb-advertisement" {
  depends_on = [
    helm_release.metallb
  ]
  wait            = true
  validate_schema = false

  yaml_body = yamlencode({
    apiVersion = "metallb.io/v1beta1"
    kind       = "L2Advertisement"
    metadata = {
      name      = "homelab-ip-advertisement"
      namespace = kubernetes_namespace_v1.metallb.id
    }
    spec = {
      ipAddressPools = [
        "homelab-ip"
      ]
    }
  })
}
