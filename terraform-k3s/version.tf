terraform {
  required_version = ">= 1.5.5"

  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "~> 4.1.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
  }
  backend "kubernetes" {
    secret_suffix = "state-gitlab"
    config_path   = "~/.kube/one.yaml"
  }
}