# CD Gitlab

Prepare

```bash
export TF_VAR_rancher2_api_url=""
export TF_VAR_rancher2_access_key=""
export TF_VAR_rancher2_secret_key=""
```

Terraform

```bash
terraform init
terraform plan
terraform apply
```
