variable "rancher2_api_url" {
  type = string
}
variable "rancher2_access_key" {
  type = string
}
variable "rancher2_secret_key" {
  type = string
}