resource "kubernetes_namespace_v1" "gitlab" {
  metadata {
    name = "gitlab"
  }
}
resource "kubectl_manifest" "gitlab-wildcard-tls" {
  depends_on = [
    kubectl_manifest.cert-manager-cluster-issuer
  ]
  wait            = true
  validate_schema = false

  yaml_body = yamlencode({
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "Certificate"
    "metadata" = {
      "name"      = "gitlab-wildcard-tls"
      "namespace" = kubernetes_namespace_v1.gitlab.metadata[0].name
    }
    "spec" = {
      "secretName" = "gitlab-wildcard-tls"
      "issuerRef" = {
        "name" = "letsencrypt-production"
        "kind" = "ClusterIssuer"
      }
      "dnsNames" = [
        "*.cloudinterplay.com"
      ]
    }
  })
}
resource "helm_release" "gitlab" {
  # depends_on = [
  #   kubectl_manifest.gitlab-wildcard-tls
  # ]
  chart      = "gitlab"
  name       = "gitlab"
  namespace  = kubernetes_namespace_v1.gitlab.metadata[0].name
  repository = "http://charts.gitlab.io"
  version    = "8.0.1"
  timeout    = 300
  values = [
    yamlencode({
      certmanager = {
        install = false
      }
      gitlab-runner = {
        runners = {
          locked = true
        }
      }
      global = {
        hosts = {
          domain = "cloudinterplay.com"
        }
        ingress = {
          configureCertmanager = false
        }
        appConfig = {
          omniauth = {
            enabled = true
            allowSingleSignOn = ["google_oauth2"]
            externalProviders = ["google_oauth2"]
            providers : [
              {
                secret = "gitlab-google-oauth2"
              }
            ]
          }
        }
      }
      gitlab = {
        gitlab-exporter = {
          enabled = false
        }
        webservice = {
          minReplicas = 1
        }
        gitaly = {
          persistence = {
            size = "40Gi"
          }
        }
      }
      registry = {
        hpa = {
          minReplicas = 1
        }
      }
      prometheus = {
        install = false
      }
      redis = {
        metrics = {
          enabled = false
        }
      }
      nginx-ingress = {
        controller = {
          ingressClassByName = true
          replicaCount       = 1
        }
      }
    })
  ]
}