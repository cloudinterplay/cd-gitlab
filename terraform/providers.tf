provider "rancher2" {
  api_url    = var.rancher2_api_url
  access_key = var.rancher2_access_key
  secret_key = var.rancher2_secret_key
  insecure   = true
}
provider "helm" {
  kubernetes {
    host                   = yamldecode(data.rancher2_cluster_v2.this.kube_config).clusters[0].cluster.server
    cluster_ca_certificate = base64decode(yamldecode(data.rancher2_cluster_v2.this.kube_config).clusters[0].cluster.certificate-authority-data)
    token                  = yamldecode(data.rancher2_cluster_v2.this.kube_config).users[0].user.token
  }
}
provider "kubernetes" {
  host                   = yamldecode(data.rancher2_cluster_v2.this.kube_config).clusters[0].cluster.server
  cluster_ca_certificate = base64decode(yamldecode(data.rancher2_cluster_v2.this.kube_config).clusters[0].cluster.certificate-authority-data)
  token                  = yamldecode(data.rancher2_cluster_v2.this.kube_config).users[0].user.token
  ignore_annotations = [
    "field.cattle.io",
    "cattle.io"
  ]
  ignore_labels = [
    "field.cattle.io"
  ]
}
provider "kubectl" {
  host                   = yamldecode(data.rancher2_cluster_v2.this.kube_config).clusters[0].cluster.server
  cluster_ca_certificate = base64decode(yamldecode(data.rancher2_cluster_v2.this.kube_config).clusters[0].cluster.certificate-authority-data)
  token                  = yamldecode(data.rancher2_cluster_v2.this.kube_config).users[0].user.token
  load_config_file       = false
}