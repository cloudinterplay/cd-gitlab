resource "kubernetes_namespace_v1" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}
resource "kubernetes_secret_v1" "cert_manager_service_account" {
  metadata {
    name      = "clouddns-dns01-solver-svc-acct"
    namespace = kubernetes_namespace_v1.cert_manager.id
  }
  data = {
    "key.json" = file("${path.module}/cert-manager-service-account.json")
  }
}
resource "helm_release" "cert_manager" {
  depends_on = [
    kubernetes_secret_v1.cert_manager_service_account,
  ]
  chart      = "cert-manager"
  name       = "cert-manager"
  namespace  = kubernetes_namespace_v1.cert_manager.metadata[0].name
  repository = "https://charts.jetstack.io"
  version    = "v1.14.5"
  timeout    = 600
  values = [
    yamlencode({
      installCRDs = true
      ingressShim = {
        defaultIssuerName = "letsencrypt-production"
      }
    })
  ]
}
resource "time_sleep" "wait_after_helm_cert_manager" {
  depends_on      = [helm_release.cert_manager]
  create_duration = "30s"
}
resource "kubectl_manifest" "cert-manager-cluster-issuer" {
  depends_on = [
    time_sleep.wait_after_helm_cert_manager
  ]
  wait            = true
  validate_schema = false

  yaml_body = yamlencode({
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-production"
    }
    "spec" = {
      "acme" = {
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        "email"  = "tools@cloudinterplay.com"
        "privateKeySecretRef" = {
          "name" = "letsencrypt-prod"
        }
        "solvers" = [
          {
            "dns01" = {
              "cloudDNS" = {
                "project" = "kubernetes-286419"
                "serviceAccountSecretRef" = {
                  "name" = kubernetes_secret_v1.cert_manager_service_account.metadata[0].name
                  "key"  = "key.json"
                }
              }
            }
          }
        ]
      }
    }
  })
}